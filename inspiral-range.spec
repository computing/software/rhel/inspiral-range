%define name inspiral-range
%define pypi_name inspiral_range
%define version 0.8.1
%define release 1

# -- metadata ---------------

BuildArch: noarch
Group:     Development/Libraries
License:   GPL-3.0-or-later
Name:      %{name}
Packager:  Duncan Macleod <duncan.macleod@ligo.org>
Prefix:    %{_prefix}
Release:   %{release}%{?dist}
Source0:   %pypi_source
Summary:   GW detector inspiral range calculation tools
Url:       https://git.ligo.org/gwinc/inspiral-range
Vendor:    California Institute of Technology
Version:   %{version}

# -- build requirements -----

# macros
BuildRequires: python-srpm-macros
BuildRequires: python-rpm-macros
BuildRequires: python3-rpm-macros

# build
BuildRequires: python3
BuildRequires: python%{python3_pkgversion}-setuptools
BuildRequires: python%{python3_pkgversion}-wheel

# test
BuildRequires: python%{python3_pkgversion}-astropy
BuildRequires: python%{python3_pkgversion}-gpstime
BuildRequires: python%{python3_pkgversion}-numpy
BuildRequires: python%{python3_pkgversion}-scipy
BuildRequires: python%{python3_pkgversion}-lal
BuildRequires: python%{python3_pkgversion}-lalsimulation

# -- packages ---------------

# inspiral-range
Requires: python%{python3_pkgversion}-%{name} = %{version}-%{release}
%description
The inspiral_range package provides tools for calculating various binary
inspiral range measures useful as figures of merit for gravitational wave
detectors characterised by a strain noise spectral density.

It includes a command-line tool for calculating various inspiral ranges
from a supplied file of detector noise spectral density (either ASD or
PSD).

This package provides the command-line utilities.

%package -n python%{python3_pkgversion}-%{name}
Summary: %{summary}
Requires: python%{python3_pkgversion}-astropy
Requires: python%{python3_pkgversion}-gpstime
Requires: python%{python3_pkgversion}-lal
Requires: python%{python3_pkgversion}-lalsimulation
Requires: python%{python3_pkgversion}-numpy
Requires: python%{python3_pkgversion}-scipy
%{?python_provide:%python_provide python%{python3_pkgversion}-%{name}}
%description -n python%{python3_pkgversion}-%{name}
The inspiral_range package provides tools for calculating various binary
inspiral range measures useful as figures of merit for gravitational wave
detectors characterised by a strain noise spectral density.

It includes a command-line tool for calculating various inspiral ranges
from a supplied file of detector noise spectral density (either ASD or
PSD).

This package provides the modules for Python %{python3_version}.

# -- build ------------------

%prep
%autosetup -n %{pypi_name}-%{version}

%build
%py3_build_wheel

%install
%py3_install_wheel %{pypi_name}-%{version}-*.whl

%clean
rm -rf $RPM_BUILD_ROOT

%check
export PATH="%{buildroot}%{_bindir}:${PATH}"
export PYTHONPATH="%{buildroot}%{python3_sitearch}:${PYTHONPATH}"
%{__python3} -m inspiral_range --asd inspiral_range/test/O2.txt
inspiral-range --asd inspiral_range/test/O3.txt m1=30 m2=30

# -- files ------------------

%files
%doc README.md
%license COPYING
%{_bindir}/*

%files -n python%{python3_pkgversion}-%{name}
%doc README.md
%license COPYING
%{python3_sitelib}/*

# -- changelog --------------

%changelog
* Tue Jun 15 2021 Duncan Macleod <duncan.macleod@ligo.org> - 0.8.1-1
- update for 0.8.1
- use wheels to build/install

* Thu May 06 2021 Duncan Macleod <duncan.macleod@ligo.org> - 0.8.0-1
- update for 0.8.0
- add python3-gpstime requirement

* Mon Jan 04 2021 Duncan Macleod <duncan.macleod@ligo.org> - 0.6.0-1
- update for 0.6.0

* Thu Jun 18 2020 Duncan Macleod <duncan.macleod@ligo.org> - 0.3.1-1
- update for 0.3.1
- add tests

* Tue Jun 16 2020 Duncan Macleod <duncan.macleod@ligo.org> - 0.3.0-1
- First packaging for 0.3.0
